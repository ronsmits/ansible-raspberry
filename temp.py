#!/usr/bin/env python3
import paho.mqtt.publish as publish
from subprocess import check_output
from re import findall

import socket

def get_temp():
    temp = check_output(["vcgencmd","measure_temp"]).decode("UTF-8")
    return(findall("\d+\.\d+",temp)[0])

def publish_message(topic, message):
    publish.single(topic, message, hostname="iot1")

temp = get_temp()
host = socket.gethostname()
publish_message("rpi/"+host+"/temp", temp)
